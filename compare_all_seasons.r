set.seed(20300057)

library(plyr)

simpsons_data <- read.csv("data/simpsons.csv", header =TRUE)
simpsons_data$Season <- as.factor(simpsons_data$Season)

ggplot(simpsons_data) + 
  geom_boxplot(
    aes(x = Season, Rating, fill = Season), show.legend=FALSE
  )

ggplot(simpsons_data, aes(x = Season)) + stat_count()

compare_multiple_gibbs <- function(y, ind, samples = 10000, mu_0 = 50, gamma_0 = 1/25, a_0 = 1/2, b_0 = 50, eta_0 = 1/2, t_0 = 50) {
  m <- nlevels(ind)
  y_bar <- tapply(y, ind, mean)
  theta <- tapply(y, ind, mean)
  tau_w <- mean(1 / tapply(y, ind, var))
  mu <- mean(theta)
  tau_b <- var(theta)
  n_m <- tapply(y, ind, length)
  a_n <- a_0 + sum(n_m)/2

  theta_matrix <- matrix(0, nrow=samples, ncol=m)
  parameters <- matrix(0, nrow=samples, ncol=3)

  for(s in 1:samples) {
    for(j in 1:m) {
      tau_n <- n_m[j] * tau_w + tau_b
      theta_n <- (y_bar[j] * n_m[j] * tau_w + mu * tau_b) / tau_n
      theta[j]<-rnorm(1, theta_n, 1/sqrt(tau_n))
    }
    sum_of_squares <- 0
    for(j in 1:m){
      sum_of_squares <- sum_of_squares + sum((y[ind == j] - theta[j])^2)
    }
    b_n <- b_0 + sum_of_squares/2
    tau_w <- rgamma(1, a_n, b_n)

    gamma_m <- m * tau_b + gamma_0
    mu <- rnorm(1, (mean(theta) * m * tau_b + mu_0 * gamma_0) / gamma_m, 1/ sqrt(gamma_m))

    eta_m <- eta_0 + m/2
    t_m <- t_0 + sum((theta - mu)^2) / 2
    tau_b <- rgamma(1, eta_m, t_m)

    theta_matrix[s,] <- theta
    parameters[s, ] <- c(mu, tau_w, tau_b)
  }
  colnames(parameters) <- c("mu", "tau_w", "tau_b")
  return(list(params = parameters, theta = theta_matrix))
}


simpsons_gibbs_model <- compare_multiple_gibbs(simpsons_data$Rating, simpsons_data$Season)


theta_df <- data.frame(
  Rating = as.numeric(simpsons_gibbs_model$theta),
  Season = rep(1:ncol(simpsons_gibbs_model$theta), each = nrow(simpsons_gibbs_model$theta))
)
theta_med <- apply(theta_df, 2, mean)

theta_df$Season <- as.factor(theta_df$Season)
ggplot(theta_df) + 
geom_boxplot(
  aes(x = Season, Rating, fill = Season), show.legend=T
)


ddply(theta_df, .(Season), summarize, mean=mean(Rating))
ddply(theta_df, .(Season), summarize, median=median(Rating))

simpsons.av <- aov(Rating ~ Season, data=simpsons_data[as.numeric(simpsons_data$Season) > 2 & as.numeric(simpsons_data$Season) <= 8,])
simpsons.av
summary(simpsons.av)

