library(MCMCpack)
library(ggplot2)
set.seed(20300057)
simpsons_data <- read.csv("data/simpsons.csv", header =TRUE)



season_2_season_6_data <- simpsons_data[simpsons_data$Season == 2 | simpsons_data$Season == 6, ]
season_2_season_6_data$Season <- as.factor(season_2_season_6_data$Season)

ggplot(season_2_season_6_data) + geom_boxplot(aes(Season, Rating, fill = Season), show.legend=T) + geom_jitter(aes(Season, Rating))

season_2_data <- simpsons_data[simpsons_data$Season == 2, ]
season_6_data <- simpsons_data[simpsons_data$Season == 6, ]


t.test(Rating ~ Season, data=season_2_season_6_data, var.equal = TRUE, alternative = "two.sided")
t.test(Rating ~ Season, data=season_2_season_6_data, var.equal = TRUE, alternative = "less")


compare_2_gibbs <- function(y, ind, mu_0 = 50, tau_0 = 1/400, delta_0 = 0, gamma_0 = 1/400, a_0 = 1, b_0 = 50, samples = 10000) {
  season_2_ratings <- y[ind == 2]
  season_6_ratings <- y[ind == 6]
  n1 <- length(season_2_ratings)
  n2 <- length(season_6_ratings)
  mu <- (mean(season_2_ratings) + mean(season_6_ratings)) / 2
  delta <- (mean(season_2_ratings) - mean(season_6_ratings)) / 2
  parameters <- matrix(0, nrow = samples, ncol = 3)
  a_n <- a_0 + (n1 + n2)/2
  for(s in 1 : samples) {
    b_n <- b_0 + 0.5 * (sum((season_2_ratings - mu - delta) ^ 2) + sum((season_6_ratings - mu + delta) ^ 2))
    tau <- rgamma(1, a_n, b_n)
    
    tau_n <- tau_0 + tau * (n1 + n2)
    mu <- rnorm(1, (tau_0 * mu_0 + tau * (sum(season_2_ratings - delta) + sum(season_6_ratings + delta))) / tau_n, sqrt(1/tau_n))
    
    gamma_n <- gamma_0 + tau*(n1 + n2)
    delta_n <- ( delta_0 * gamma_0 + tau * (sum(season_2_ratings - mu) - sum(season_6_ratings - mu))) / gamma_n
    delta <- rnorm(1, delta_n, sqrt(1/gamma_n))
    
    parameters[s, ] <- c(mu, delta, tau)
  }
  colnames(parameters) <- c("mu", "delta", "tau")
  return(parameters)
}

simpsons_gibbs_model <- compare_2_gibbs(season_2_season_6_data$Rating, season_2_season_6_data$Season)
plot(as.mcmc(simpsons_gibbs_model))
raftery.diag(as.mcmc(simpsons_gibbs_model))
apply(simpsons_gibbs_model, 2, mean)
apply(simpsons_gibbs_model, 2, sd)
mean(1/sqrt(simpsons_gibbs_model[, 3]))

sd(1/sqrt(simpsons_gibbs_model[, 3]))
y1_simulate <- rnorm(10000, simpsons_gibbs_model[, 1] + simpsons_gibbs_model[, 2], sd = 1/sqrt(simpsons_gibbs_model[, 3]))
y2_simulate <- rnorm(10000, simpsons_gibbs_model[, 1] - simpsons_gibbs_model[, 2], sd = 1/sqrt(simpsons_gibbs_model[, 3]))
ggplot(data.frame(y_simulated_difference = y1_simulate - y2_simulate)) + stat_bin(aes(y_simulated_difference))

mean(y1_simulate < y2_simulate)


