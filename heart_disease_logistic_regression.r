set.seed(20300057)

library(corrplot)
library(rstanarm)
library(bayesplot)
library(glmnet)
library(caret)
library(ROCR)

heart_data <- read.csv("data/heart_data.csv")
folds <- createFolds(heart_data$Class, k = 10)

heart_data$Class <- as.factor(heart_data$Class)
heart_data$Sex <- as.factor(heart_data$Sex)
heart_data$FastingBloodSugar <- as.factor(heart_data$FastingBloodSugar)
heart_data$ExerciseInduced <- as.factor(heart_data$ExerciseInduced)
heart_data$Slope <- factor(heart_data$Slope, order = TRUE, levels = c("1", "2", "3")) 

heart_data[c(1,3,4,6,9)] <- apply(heart_data[c(1,3,4,6,9)], 2, scale)

corrplot(cor(heart_data[c(1,3,4,6,9)]), method="circle")
cor(heart_data[c(1,3,4,6,9)])

# mcmc bayesian logistc regression
bayesian_mcmc_heart_glm <- stan_glm(Class ~ ., data = heart_data, family = binomial())

plot(bayesian_mcmc_heart_glm, plotfun = "trace")
plot(bayesian_mcmc_heart_glm, plotfun = "dens")
summary(bayesian_mcmc_heart_glm, digits = 5)

bayesian_mcmc_heart_glm_params <- as.data.frame(bayesian_mcmc_heart_glm)
cor(bayesian_mcmc_heart_glm_params)[cor(bayesian_mcmc_heart_glm_params) >= 0.4 | cor(bayesian_mcmc_heart_glm_params) <= -0.4]
corrplot(cor(bayesian_mcmc_heart_glm_params), method="circle")


k_fold_results <- lapply(folds, function(x) {
  bayesian_mcmc_heart_glm <- stan_glm(Class ~ ., data = heart_data[-x,], family = binomial(), )
  pred <- predict(bayesian_mcmc_heart_glm, newdata = heart_data[x, -length(heart_data)], type="response")
  
  pred_labels = as.factor(ifelse(as.vector(pred) > 0.5, "2", "1"))
  result_metrics <- confusionMatrix(pred_labels, heart_data[x, length(heart_data)], mode="prec_recall", positive = "2")
  auc <- performance(prediction(predictions=pred, labels=heart_data[x, length(heart_data)]), measure = "auc")
  
  return(
    data.frame(
      auc=unlist(auc@y.values), 
      recall=result_metrics$byClass["Recall"], 
      accuracy=result_metrics$overall["Accuracy"]
    )
  )
})


mean_recall <- mean(unlist(lapply(k_fold_results, function(x) x["recall"])))
mean_auc <- mean(unlist(lapply(k_fold_results, function(x) x["auc"])))
mean_acc <- mean(unlist(lapply(k_fold_results, function(x) x["accuracy"])))


print(paste("mean recall", mean_recall))
print(paste("mean accuracy", mean_acc))
print(paste("mean auc", mean_auc))


# frequentist logisitic regressiion
heart_freq_glm <- glm(Class ~ ., data = heart_data, family = binomial())

k_fold_results <- lapply(folds, function(x) {
  heart_freq_glm <- glm(Class ~ ., data = heart_data, family = binomial())
  pred <- predict(heart_freq_glm, newdata = heart_data[x, -length(heart_data)], type="response")
  
  pred_labels = as.factor(ifelse(as.vector(pred) > 0.5, "2", "1"))
  result_metrics <- confusionMatrix(pred_labels, heart_data[x, length(heart_data)], mode="prec_recall", positive = "2")
  auc <- performance(prediction(predictions=pred, labels=heart_data[x, length(heart_data)]), measure = "auc")
  return(
    data.frame(
      auc=unlist(auc@y.values), 
      recall=result_metrics$byClass["Recall"], 
      accuracy=result_metrics$overall["Accuracy"]
    )
  )
})

mean_recall <- mean(unlist(lapply(k_fold_results, function(x) x["recall"])))
mean_auc <- mean(unlist(lapply(k_fold_results, function(x) x["auc"])))
mean_acc <- mean(unlist(lapply(k_fold_results, function(x) x["accuracy"])))

print(paste("mean recall", mean_recall))
print(paste("mean accuracy", mean_acc))
print(paste("mean auc", mean_auc))

summary(heart_freq_glm)


# lasso logistic regression
heart_x_data <- data.matrix(heart_data[-length(heart_data)])
heart_y_data <- factor(heart_data$Class)

heart_lasso_glm <- glmnet(heart_x_data, heart_y_data, family = "binomial")
plot(heart_lasso_glm, label = TRUE)


heart_lasso_glmcv <- cv.glmnet(heart_x_data, heart_y_data, family = "binomial")
summary(heart_lasso_glmcv)
coef(heart_lasso_glmcv, s="lambda.min")
plot(heart_lasso_glmcv)


lambda_value <- heart_lasso_glmcv$lambda.min


k_fold_results <- lapply(folds, function(x) {
  heart_x_data <- heart_data[,-length(heart_data)]
  
  heart_y_data <- factor(heart_data$Class[-x])
  heart_lasso_glm <- glmnet(heart_x_data[-x,], heart_y_data, family = "binomial")
  pred <- predict(heart_lasso_glm, data.matrix(heart_x_data[x,]), s=lambda_value)
  
  pred_labels = as.factor(ifelse(as.vector(pred) > 0.5, "2", "1"))
  
  result_metrics <- confusionMatrix(pred_labels, heart_data$Class[x], mode="prec_recall", positive = "2")
  auc <- performance(prediction(predictions=pred, labels=heart_data[x, length(heart_data)]), measure = "auc")
  return(
    data.frame(
      auc=unlist(auc@y.values), 
      recall=result_metrics$byClass["Recall"], 
      accuracy=result_metrics$overall["Accuracy"]
    )
  )
})


mean_auc <- mean(unlist(lapply(k_fold_results, function(x) x["auc"])))
mean_recall <- mean(unlist(lapply(k_fold_results, function(x) x["recall"])))
mean_acc <- mean(unlist(lapply(k_fold_results, function(x) x["accuracy"])))
print(paste("mean recall", mean_recall))
print(paste("mean accuracy", mean_acc))
print(paste("mean auc", mean_auc))
print(lambda_value)



# stepwise logistic regression
heart_data$Class <- factor(heart_data$Class)
heart_stepwise_glm <- glm(Class ~ ., data = heart_data, family = binomial())

k_fold_results <- lapply(folds, function(x) {
  heart_stepwise_glm <- glm(Class ~ ., data = heart_data, family = binomial())
  heart_stepwise_AIC_backward <- step(heart_stepwise_glm, direction = "backward")
  pred <- predict(heart_stepwise_AIC_backward, newdata = heart_data[x, -length(heart_data)], type="response")
  
  pred_labels = as.factor(ifelse(as.vector(pred) > 0.5, "2", "1"))
  result_metrics <- confusionMatrix(pred_labels, heart_data[x, length(heart_data)], mode="prec_recall", positive = "2")
  auc <- performance(prediction(predictions=pred, labels=heart_data[x, length(heart_data)]), measure = "auc")
  return(
    data.frame(
      auc=unlist(auc@y.values), 
      recall=result_metrics$byClass["Recall"], 
      accuracy=result_metrics$overall["Accuracy"]
    )
  )
})

mean_recall <- mean(unlist(lapply(k_fold_results, function(x) x["recall"])))
mean_auc <- mean(unlist(lapply(k_fold_results, function(x) x["auc"])))
mean_acc <- mean(unlist(lapply(k_fold_results, function(x) x["accuracy"])))

print(paste("mean recall", mean_recall))
print(paste("mean accuracy", mean_acc))
print(paste("mean auc", mean_auc))

heart_stepwise_AIC_backward <- step(heart_stepwise_glm, direction = "backward")
summary(heart_stepwise_AIC_backward)



heart_data_removed_age <- heart_data[, c(-1)]

# mcmc bayesian logistc regression
bayesian_mcmc_heart_glm <- stan_glm(Class ~ ., data = heart_data_removed_age, family = binomial())
summary(bayesian_mcmc_heart_glm, digits = 5)

folds <- createFolds(heart_data_removed_age$Class, k = 10)

k_fold_results <- lapply(folds, function(x) {
  bayesian_mcmc_heart_glm <- stan_glm(Class ~ ., data = heart_data_removed_age[-x,], family = binomial(), )
  pred <- predict(bayesian_mcmc_heart_glm, newdata = heart_data_removed_age[x, -length(heart_data_removed_age)], type="response")
  
  pred_labels = as.factor(ifelse(as.vector(pred) > 0.5, "2", "1"))
  result_metrics <- confusionMatrix(pred_labels, heart_data_removed_age[x, length(heart_data_removed_age)], mode="prec_recall", positive = "2")
  auc <- performance(prediction(predictions=pred, labels=heart_data_removed_age[x, length(heart_data_removed_age)]), measure = "auc")
  
  return(
    data.frame(
      auc=unlist(auc@y.values), 
      recall=result_metrics$byClass["Recall"], 
      accuracy=result_metrics$overall["Accuracy"]
    )
  )
})



mean_recall <- mean(unlist(lapply(k_fold_results, function(x) x["recall"])))
mean_auc <- mean(unlist(lapply(k_fold_results, function(x) x["auc"])))
mean_acc <- mean(unlist(lapply(k_fold_results, function(x) x["accuracy"])))


print(paste("mean recall", mean_recall))
print(paste("mean accuracy", mean_acc))
print(paste("mean auc", mean_auc))


# frequentist logisitic regressiion
heart_freq_glm <- glm(Class ~ ., data = heart_data_removed_age, family = binomial())


k_fold_results <- lapply(folds, function(x) {
  heart_freq_glm <- glm(Class ~ ., data = heart_data_removed_age, family = binomial())
  pred <- predict(heart_freq_glm, newdata = heart_data_removed_age[x, -length(heart_data_removed_age)], type="response")
  
  pred_labels = as.factor(ifelse(as.vector(pred) > 0.5, "2", "1"))
  result_metrics <- confusionMatrix(pred_labels, heart_data_removed_age[x, length(heart_data_removed_age)], mode="prec_recall", positive = "2")
  auc <- performance(prediction(predictions=pred, labels=heart_data_removed_age[x, length(heart_data_removed_age)]), measure = "auc")
  return(
    data.frame(
      auc=unlist(auc@y.values), 
      recall=result_metrics$byClass["Recall"], 
      accuracy=result_metrics$overall["Accuracy"]
    )
  )
})

mean_recall <- mean(unlist(lapply(k_fold_results, function(x) x["recall"])))
mean_auc <- mean(unlist(lapply(k_fold_results, function(x) x["auc"])))
mean_acc <- mean(unlist(lapply(k_fold_results, function(x) x["accuracy"])))

print(paste("mean recall", mean_recall))
print(paste("mean accuracy", mean_acc))
print(paste("mean auc", mean_auc))

summary(heart_freq_glm)



# lasso logistic regression
heart_x_data <- data.matrix(heart_data_removed_age[-length(heart_data_removed_age)])
heart_y_data <- factor(heart_data_removed_age$Class)

heart_lasso_glm <- glmnet(heart_x_data, heart_y_data, family = "binomial")

heart_lasso_glmcv <- cv.glmnet(heart_x_data, heart_y_data, family = "binomial")
summary(heart_lasso_glmcv)

lambda_value <- heart_lasso_glmcv$lambda.min


k_fold_results <- lapply(folds, function(x) {
  heart_x_data <- heart_data_removed_age[,-length(heart_data_removed_age)]
  
  heart_y_data <- factor(heart_data_removed_age$Class[-x])
  heart_lasso_glm <- glmnet(heart_x_data[-x,], heart_y_data, family = "binomial")
  pred <- predict(heart_lasso_glm, data.matrix(heart_x_data[x,]), s=lambda_value)
  
  pred_labels = as.factor(ifelse(as.vector(pred) > 0.5, "2", "1"))
  
  result_metrics <- confusionMatrix(pred_labels, heart_data_removed_age$Class[x], mode="prec_recall", positive = "2")
  auc <- performance(prediction(predictions=pred, labels=heart_data_removed_age[x, length(heart_data_removed_age)]), measure = "auc")
  return(
    data.frame(
      auc=unlist(auc@y.values), 
      recall=result_metrics$byClass["Recall"], 
      accuracy=result_metrics$overall["Accuracy"]
    )
  )
})


mean_auc <- mean(unlist(lapply(k_fold_results, function(x) x["auc"])))
mean_recall <- mean(unlist(lapply(k_fold_results, function(x) x["recall"])))
mean_acc <- mean(unlist(lapply(k_fold_results, function(x) x["accuracy"])))
print(paste("mean recall", mean_recall))
print(paste("mean accuracy", mean_acc))
print(paste("mean auc", mean_auc))
print(lambda_value)


# stepwise logistic regression
heart_data_removed_age$Class <- factor(heart_data_removed_age$Class)
heart_stepwise_glm <- glm(Class ~ ., data = heart_data_removed_age, family = binomial())
summary(heart_stepwise_glm)

k_fold_results <- lapply(folds, function(x) {
  heart_stepwise_glm <- glm(Class ~ ., data = heart_data_removed_age, family = binomial())
  heart_stepwise_AIC_backward <- step(heart_stepwise_glm, direction = "backward")
  pred <- predict(heart_stepwise_AIC_backward, newdata = heart_data_removed_age[x, -length(heart_data_removed_age)], type="response")
  
  pred_labels = as.factor(ifelse(as.vector(pred) > 0.5, "2", "1"))
  result_metrics <- confusionMatrix(pred_labels, heart_data_removed_age[x, length(heart_data_removed_age)], mode="prec_recall", positive = "2")
  auc <- performance(prediction(predictions=pred, labels=heart_data_removed_age[x, length(heart_data_removed_age)]), measure = "auc")
  return(
    data.frame(
      auc=unlist(auc@y.values), 
      recall=result_metrics$byClass["Recall"], 
      accuracy=result_metrics$overall["Accuracy"]
    )
  )
})

mean_recall <- mean(unlist(lapply(k_fold_results, function(x) x["recall"])))
mean_auc <- mean(unlist(lapply(k_fold_results, function(x) x["auc"])))
mean_acc <- mean(unlist(lapply(k_fold_results, function(x) x["accuracy"])))

print(paste("mean recall", mean_recall))
print(paste("mean accuracy", mean_acc))
print(paste("mean auc", mean_auc))


heart_stepwise_AIC_backward <- step(heart_stepwise_glm, direction = "backward")
summary(heart_stepwise_AIC_backward)

