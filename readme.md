# Main Project CS7DS3 Applied Statistical Modelling
MSc Computer Science - Intelligent Systems <br/>
Name: Joshua Cassidy<br/>
Student Number: 20300057<br/>
Module: CS7DS3 Applied Statistical Modelling<br/>


# Project Dependencies:
1. R (version 3.6.3)
3. ggplot2
4. corrplot
5. rstanarm
6. bayesplot
7. glmnet
8. caret
9. ROCR
10. plyr

# Running the Project
The following steps detail how to run the Main Project:
1. Ensure R and its dependancies are installed
2. Open the project up in the downloaded directory (Ensure the R path is in the downloaded directory this can be changed with the setwd() function)
3. Run the compare_simpsons_season_2_season_6.r for the output and graph files for question 1a
4. Run the compare_all_seasons.r for the output and graph files for question 1b
5. Run the heart_disease_logistic_regression.r for the output and graph files for question 2a

# Questions answered
The questions that were answered for the purposes of this project are:<br/>
1a. The code supplementry code for 1a can be found in compare_simpsons_season_2_season_6.r<br/>
1b. The code supplementry code for 1b can be found in compare_all_seasons.r<br/>
2a. The code supplementry code for 2a can be found in heart_disease_logistic_regression.r<br/>
